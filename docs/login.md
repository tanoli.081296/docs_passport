##Passport Integration

_|_|
-------|-|
Version| 1.1
Release date| 22-03-2018
Description| This document specifies the APIs of VNG System provides for game to integrate with it

##Change log
Version|Description|
-|-|

##List APIs

_|Name|Description
-|----|-----------
1|[API submit Profile](#1-api-submit-profile) | - 
2|[API gen form submit Profile](#2-api-gen-form-submit-profile) | -

<div class="pagebreak"></div>

###1. API submit Profile

* Url: https://login.pp.m.zing.vn/profile/extinfo/submit
* Protocol: Http POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    appID `required`|String|ID of game (assigned by VNG)
    userID `required`|String|ID of user
    timestamp `required`|String|Current time in milliseconds
    **name** `required`|String|Name of user<br/> Required regex: `^[A-Za-z0-9_\\.]{3,24}$`
    **ppID** `required`|String|Passport id of user(CMND, CCCD, Passport)<br/> Required regex: `^[a-zA-Z0-9]{8,15}$`
    **email** `required`|String|Email of user<br/> Required regex: `^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$`
    **phone** `required`|String|Phone number of user<br/> Required regex: `[0|84]\\d{9,10}`
    **address** `required`|String|Address of user<br/> Required regex: `^.{0,64}$`
    sig `required`|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code  
    message|String|Error description

* Return code/message:

    Code|Description
    ----|----------
    0|Thành công
    1|Dữ liệu không chính xác
    2|Dữ liệu không hợp lệ (sai signature)
    500|Hệ thống xảy ra lỗi
    ...|...

    
* Example request:
    
    ```
    curl "https://login.pp.m.zing.vn/profile/extinfo/submit" \ 
    -X POST \
    -H "Content-type: application/x-www-form-urlencoded" \
    -d 'appID=pubgm&userID=981236928&sig=a4ab2f7c555745abd0d48a8b6e40e4a4&timestamp=1539171798629&name=locth2&ppID=024141947&email=locth2%40vng.com.vn&phone=0934482997&address=Q5+Ho+Chi+Minh'
    
    {"returnCode": 0,"message": "Thành công"}
    ```

###2. API gen form submit Profile

* Url: https://login.pp.m.zing.vn/profile/extinfo
* Protocol: Http GET
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    appID `required`|String|ID of game (assigned by VNG)
    userID `required`|String|ID of user
    timestamp `required`|String|Current time in milliseconds
    sig `required`|String|```md5($SECRET_KEY+ $appID +$timestamp+ $userID)```<br>$SECRET_KEY: will be provided after register integration

<div class="pagebreak"></div>
* Example request: https://login.pp.m.zing.vn/profile/extinfo?appID=pubgm&userID=981236928&timestamp=1539171798629&sig=a4ab2f7c555745abd0d48a8b6e40e4a4
    
    
<div class="pagebreak"></div>

##Appendix 1. Create signature

* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive. 

* All leading and trailing whitespace of a string will have to be trimmed.  

* Example:

    ```
    URL: "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item1&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&distSrc=pc&timestamp=1517900546676";
    Secret key: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    Raw: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "game" + "item" + "1" + "6789" + "test" + "11517900546676" + "12345"
    Sig: md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcgameitem16789test1151790054667612345") = "6d1ecb0d9e9d118e032f9c8b5f0ad866"
    ```
