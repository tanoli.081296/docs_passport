##Passport Integration

| -            | -                                                                                                                                                                  |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Version      | 1.3                                                                                                                                                                |
| Release date | 10-12-2020                                                                                                                                                         |
| Description  | This document specifies the APIs of VNG System provides for game to integrate with it                                                                              |
| Change note  | - Regex validate "phone" in "API submit Profile"<br> - Validate "dob" and "ppDOI" in "API submit Profile"<br>- Allow character "address" in "API submit Profile"<br>- Return code/message all API<br>- Appendix 1. Vietnamese character |


##List APIs

_|Name|Description
:-|:----|:-----------
1|[API submit Profile](#1-api-submit-profile) |This API provided by VNG and used by every Game client to submit user profile (personal) info whenever the user registers a new account of the game.The reason of having this API calls in Game client is due to requirement of Vietnamese regulation in which VNG has to store enough personal information of every player's account of every game. 
2|[URL to request the form updatingProfile](#2-url-to-request-the-form-updatingprofile) |The updating profile web-form is hosted in VNG side. This section is to describe how to compose a URL with necessary parameters to load the web-form for allowing user updates his/her profile info. In case of Riot integration, Riot will only load this form in PC game client version.
3|[API submit Login log](#3-api-submit-login-log) |Whenever a user/player does login to game, the Game system needs to push data to VNG by calling this API.
4|[API get Profile](#3-api-get-profile) |This API provided by VNG to Riot get user Profile.

<div class="pagebreak"></div>

###1. API submit Profile

* Url: https://login.pp.m.zing.vn/profilev2/extinfo/submit
* Protocol: Http POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    appID `required`|String|ID of App. VNG will provide appID when Riot & VNG begin to intergrate.  Ex: lor
    userID `required`|String|Riot's UUID
    timestamp `required`|String|Current time in milliseconds
    **name** `required`|String|Real name of the user.<br/>String format accepts Vietnamese character (Unicode 32) and digit character. Maximum length is 30 characters. [Appendix 1](#appendix-1-vietnamese-character)
    **ppID** `required`|String|Passport id of user(CMND, CCCD, Passport)<br/> Required regex: `^[a-zA-Z0-9]{8,15}$`
    **email** `optional`         |String|Email of user<br/> Required regex: `^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$`
    **phone** `required`|String|Phone number of user<br/> Required regex: `^(84|\\+84|0|0084)\\d{9}$`
    **address** `optional`        |String|Address of user<br/> Accept Vietnamese character, number and special character "/" "," "-" with length < 50 [Appendix 1](#appendix-1-vietnamese-character)
    **dob** `required` |String|Birthday of user<br/> Format `dd/MM/yyyy`. The day in future is not accepted.
    **ppDOI** `required`|String|Date of issue<br/> Format `dd/MM/yyyy`. The day in future is not accepted and after Birthday of user.
    sig `required`|String|Referring to signature generation in [Appendix 2](#appendix-2-create-signature) <br/> VNG will provide secretKey when Riot & VNG begin to intergrate


* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code  
    message|String|Error description

* Return code/message:

    Code|Description
    ----|----------
    0 |Successful
    1 |Integrity error (signature checking fail)
    2 |Invalid format or parameters
    11|Invalid email
    12|Invalid phone
    13|Invalid personal ID
    14|Invalid date time
    15|Invalid address
    16|Invalid name
    500|System error
    ...|...    

###2. URL to request the form updatingProfile

* Prefix url: https://login.pp.m.zing.vn/profilev2/extinfo
* Protocol: Http GET
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    appID `required`|String|ID of App. VNG will provide appID when Riot & VNG begin to intergrate.  Ex: lor
    userID `required`|String|Riot's UUID 
    timestamp `required`|String|Current time in milliseconds
    sig `required`|String|Referring to signature generation in [Appendix 2](#appendix-2-create-signature) <br/> VNG will provide secretKey when Riot & VNG begin to intergrate
    

###3. API submit Login log

* Url: https://login.pp.m.zing.vn/log/login/submit
* Protocol: Http POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:


    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    appID `required`|String|ID of App. VNG will provide appID when Riot & VNG begin to intergrate.  Ex: lor
    userID `required`|String|Riot's UUID 
    userIP `required`|String|IP of user login <br/> check IP format
    timestamp `required`|String|Current time in milliseconds
    sig `required`|String|Referring to signature generation in [Appendix 2](#appendix-2-create-signature) <br/> VNG will provide secretKey when Riot & VNG begin to intergrate

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code  
    message|String|Error description

* Return code/message:

    Code|Description
    ----|----------
    0|Successful
    1|Integrity error (signature checking fail)
    2|Invalid format or parameters
    500|System error
    ...|...

###4. API get Profile

* Url: https://login.pp.m.zing.vn/profilev2/extinfo/get
* Protocol: Http POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    appID `required`|String|ID of App. VNG will provide appID when Riot & VNG begin to intergrate.  Ex: lor
    userID `required`|String|Riot's UUID 
    timestamp `required`|String|Current time in milliseconds
    sig `required`|String|Referring to signature generation in [Appendix 2](#appendix-2-create-signature) <br/> VNG will provide secretKey when Riot & VNG begin to intergrate

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code  
    message|String|Error description
    data|JSON Object|Format: <br>```{```<br>&emsp;```"name" : String,```<br>&emsp;```"dob" : String dd/MM/yyyy,```<br>&emsp;```"ppID" : String,```<br>&emsp;```"ppDOI" : String dd/MM/yyyy```<br>&emsp;```"address" : String,```<br>&emsp;```"email" : String,```<br>&emsp;```"phone" : String,```<br>```}```

* Return code/message:

    Code|Description
    ----|----------
    0|Successful
    1|Integrity error (signature checking fail)
    2|IInvalid format or parameters
    10|user's Profile not existed
    500|System error
    ...|...

##Appendix 1. Vietnamese character 
[A-Za-z0-9_\.'áàảãạÁÀẢÃẠăắằẳẵặĂẮẰẲẴẶâấầẩẫậÂẤẦẨẪẬđĐ
éèẻẽẹÉÈẺẼẸêếềểễệÊẾỀỂỄỆíìỉĩịÍÌỈĨỊýỳỷỹỵÝỲỸỶỴ
ưứừửữựƯỨỪỬỮỰúùủũụÚÙỦŨỤóòỏõọôốồổỗộơớờởỡợÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]

##Appendix 2. Create signature

- Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

- Secret Key is a server-side shared key. VNG will provide secretKey when Riot & VNG begin to intergrate.

- All parameters except the signature itself of an exchange message will participate to form the signature.

- All parameter values that form the signature must sort alphabetically based on parameter name.

- All parameters that form the signature must in their original format (i.e., not a URL encoded).

- All parameters that form the signature are case sensitive.

- All leading and trailing whitespace of a string will have to be trimmed.

URL: "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item
&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&timestamp=1517900546676";
Secret key: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
Raw: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "game" + "item" + "1" + "6789" + "test" + "11517900546676" + "12345"
Sig: md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcgameitem16789test1151790054667612345") = "6d1ecb0d9e9d118e032f9c8b5f0ad866"
