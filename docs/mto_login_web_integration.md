##MTO Login Web Integration

_|_|
-------|-|
Version| 1.0
Release date| 22-03-2018
Description| MTO Login web is system used for **app has been integrated** with Login SDK (Mto, Zalo, Zing) and social (facebook , google) could authenticate user on web platform

##Change log
Version|Description|
-|-|

##Diagram

* Login app integrated:

```mermaid
sequenceDiagram
Client->>PP Login Web:request login form

activate PP Login Web
PP Login Web->>Social:get Authentication URL

activate Social
Social-->>PP Login Web: return
deactivate Social

activate Client
PP Login Web-->>Client:(1)
deactivate PP Login Web

Note left of PP Login Web:(1) - Return Authentication Url (302 HTTP)
Client->>Social:Access Authen URL + Approve access permission

activate Social
Social-->>Client:Return callback URL of Authen result (302 HTTP)
deactivate Social

Client->>PP Login Web:access callback URL of result

activate PP Login Web
PP Login Web->>Social:verify code

activate Social
Social-->>PP Login Web:return
PP Login Web->>Social:(2)
Note right of PP Login Web:(2) - Get Access Token + graph if success verification
Social-->>PP Login Web:return
deactivate Social

opt app integrated with MTO, Zalo, Zing
    PP Login Web->>User Platform:Verify & get Profile
    activate User Platform
    User Platform-->>PP Login Web:return
    deactivate User Platform
end

PP Login Web-->>Client:(3)
deactivate PP Login Web

Note left of PP Login Web:(3) - Return callback URL to App with userID & sessionID (302 HTTP)
Client->>App:access to App

activate App
App->>PP Login Web:verify SessionID

activate PP Login Web
PP Login Web-->>App:return
deactivate PP Login Web

App-->>Client:return
deactivate App

deactivate Client
```

##Integration

###1. Information required for integration

- Game name:
- Game shortname: $gameID
- Callback ID: $mapID
- Callback URL:
Social Info:
- Facebook Channel:
    + Facebook appID
    + Facebook app secret key
    + config Social Login Redirect URL to https://login.pp.m.zing.vn/login/fbcallback/$gameID
- Google Channel:
    + oAuth web clientID
    + oAuth web client secret key
    + config Authorized Redirect Urls: https://login.pp.m.zing.vn/login/ggcallback/$gameID
- Zalo Channel:
    + zalo appID
    + zalo app secretKey
    + config RedirectUrl: https://login.pp.m.zing.vn/login/ggcallback/$gameID
    + Android package name
    + Android hash key ( required if app use ZaloSDK)

###2. Login Form URL Format

- Zing: ```https://login.pp.m.zing.vn/oauth/zing?appID=$gameID&mapID=$mapID```
- Facebook: ```https://login.pp.m.zing.vn/oauth/facebook?appID=$gameID&mapID=$mapID```
- Zalo: ```https://login.pp.m.zing.vn/oauth/zalo?appID=$gameID&mapID=$mapID```
- Google: ```https://login.pp.m.zing.vn/oauth/google?appID=$gameID&mapID=$mapID```
- Email: ```https://login.pp.m.zing.vn/oauth/email?appID=$gameID&mapID=$mapID```

###3. API

####3.1 Check Session
    
* Url: https://login.pp.m.zing.vn/login/session/check
* Protocol: HTTP GET / POST
* Description: Check session in passport system (for web app), if your app uses other session, please check other docs.
* Input params:

    Param|Type|Description
    -----|----|-----------
    appID `required`|String|```$gameID```
    userID `required`|String|userID will be include in URL callback after successful login.
    sessionID `required`|String|sessionID will be included in URL callback after successful login
    userAgent `required`|String|user agent of browser/device
    timestamp `required`|String|time in milisecond
    sig `required`|String|```md5($SECRET_KEY + $gameID + $userID + $sessionID + $userAgent + $timestamp)```<br>```$SECRET_KEY```: will be providedafter register integration

* Output params:

    Param|Type|Description
    -----|----|-----------
    returnCode|Integer| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    message|String| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)

####3.2 Remove Session

* Url: https://login.pp.m.zing.vn/login/session/remove
* Protocol: HTTP GET / POST
* Description: Remove session in passport system (passport system can't remove permisson which accessed by user in social system).
* Input params:

    Param|Type|Description
    -----|----|-----------
    appID `required`|String|```$gameID```
    userID `required`|String|userID will be include in URL callback after successful login.
    sessionID `required`|String|sessionID will be included in URL callback after successful login
    timestamp `required`|String|time in milisecond
    sig `required`|String|```md5($SECRET_KEY + $gameID + $userID + $sessionID + $timestamp)```<br>```$SECRET_KEY```: will be providedafter register integration

* Output params:

    Param|Type|Description
    -----|----|-----------
    returnCode|Integer| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    message|String| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    
####3.3 Get Profile

* Url: https://login.pp.m.zing.vn/login/session/getProfile
* Protocol: HTTP GET / POST
* Input params:

    Param|Type|Description
    -----|----|-----------
    appID `required`|String|```$gameID```
    sessionID `required`|String|sessionID will be included in URL callback after successful login
    timestamp `required`|String|time in milisecond
    sig `required`|String|```md5($SECRET_KEY + $gameID + $sessionID + $timestamp)```<br>```$SECRET_KEY```: will be providedafter register integration

* Output params:

    Param|Type|Description
    :-----|:----|:----
    returnCode|Integer| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    message|String| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    data|JSON Object| Format: <br>```{```<br>&emsp;```"loginType" : String,```<br>&emsp;```"name" : String,```<br>&emsp;```"avatar" : String,```<br>&emsp;```"accessToken" : String,```<br>&emsp;```"socialInfo" : { ```<br>&emsp;&emsp;```"$optionalInfo": ""```<br>&emsp;```},```<br>&emsp;```"userID" :  String```<br>```}```

###Appendix 1. returnCode and message

* Description table
    
    returnCode|message
    ----|-------------
    0   | Success,
    1   | Invalid signature,
    2   | Invalid input data,
    3   | Expired TTL
    4   | AppID not exists
    5   | MapID not exists
    9   | Api not supported
    100 | Back to login
    101 | Permission denied by social
    102 | Email exists
    103 | Email not exists104: Non existed user
    106 | Invalid username or password
    107 | Social login error
    108 | Session invalid or expired
    109 | Login channel not supported
    301 | Invalid Configuration
    500 | Server error





