# Login Gateway

## 1. Mô tả dịch vụ
* Là dịch vụ trung gian kết nối đến các Login Platform (Zalo SDK cũ, GS Pasport, Talk ...)
* Cung cấp tính năng login web cho các dịch vụ hỗ trợ game mobile
    * Webpay: Võ Lâm Truyền Kì Mobile, ... <a href="https://pay.zing.vn/wplogin/mobile/jxm" target="_blank">Link</a>
    * Promotion: Mu Awaken,... <a href="http://new.khuyenmai.zing.vn/mua/code" target="_blank">Link</a>
    * CS: Hỗ Trợ Khách Hàng <a href="https://hotro.zing.vn" target="_blank">Link</a>
    * Webview cho game client: Liệt Hỏa Như Ca, ... <a href="https://login.pp.m.zing.vn/desktop/lhnc" target="_blank">Link</a>
* Hỗ trợ cập nhập thông tin user cho các game chỉ cần giấy phép mà không cần sử dụng phương thức đăng nhập Zing (pubgm,...)

## 2. Flow chart hoạt động
![LoginGW Flow](LoginGW-Flow.drawio)

## 3. Phân loại dịch vụ
### 3.1 User Platform integration
#### 3.1.1 Mô tả
Đối với những game sử dụng logic đặc thù để định danh user (không dùng trực tiếp socialID của các kênh Social như FB,GG,Zalo,... để định danh user). Cần cung cấp API để GT có thể tiến hành tích hợp LoginGW với API game nhằm trả về client userID chính xác của user đã đăng nhập.
#### 3.1.2 Flow hoạt động
![GetUserPlatformProfile Flow](GetUserPlatformProfile-Flow.drawio)
#### 3.1.3 APIs
* Protocol: HTTP GET / POST
* Description: `API Tham khảo`Nhận social Profile của user cũng như các thông tin xác thực với bên Social và trả về user Platform Profile.
* Input params:

    Param|Type|Description
    -----|----|-----------
    gameID `required`|String| GameID được đinh nghĩa của hệ thống userPlatform .
    socialID `required`|String| userID được định nghĩa của hệ thống Social mà user đăng nhập (FB, GG, Zalo).
    authenCode `required`|String| Code được hệ thống Social cung cấp sau khi người dùng đăng nhập dùng để  laays thông tin `access_token` (Tham khảo chuẩn OAuth 2.0 để biết thêm chi tiết)
    timestamp `required`|String| Biến thời gian theo mili giây nhằm xác định thời gian request được gọi tới.
    sig `required`|String| Dữ liệu nhằm để  đảm bảo tính toàn vẹn của dữ liệu cũng như xác thực đối với bên gọi VD:```md5($SECRET_KEY + $gameID + $userID + $sessionID + $userAgent + $timestamp)```<br>```$SECRET_KEY```: Sẽ được cung cấp mật giữ 2 bên.


### 3.2 Web integration
#### 3.1.1 Mô tả
Sau khi GT hòan tất việc tích hợp với userPlatform của game (đối với những game sử dụng userPlatform đặc biệt). GT sẽ tiến hành việc cấu hình và tích hợp login web đối với các dịch vụ web (webpay, promotion, cs, ...).
#### 3.1.2 Cung cấp thông tin
##### 3.1.2.1 Các bước tạo yêu cầu & cung cấp thông tin 3rd.
###### Tích hợp login Social
Những thông tin cần thiết để tích hợp login Social là **`Client ID, Secret Key`** (*Liên hệ team PASS Store để được hỗ trợ* - contact: MinhGV).

Key work search: *Get Google/Facebook app Client ID and Client Secret*.

Video tham khảo:

* Google: https://www.youtube.com/watch?v=Jg__b_WM5gg
* Facebook: https://www.youtube.com/watch?v=kQ4s-8Cb5-o
* Zalo: https://developers.zalo.me/docs/api/social-api/tai-lieu/bat-dau-nhanh-post-1011

Hiện tại GT đang hỗ trợ 2 hình thức tích hợp:

* Phía đối tác sẽ chủ động tích hợp với các kênh login mong muốn (Zing, Facebook, Google, ...) tạo ra `UserPlatform` riêng của đối tác. Sau đó cung cấp cấu trúc API để GT tiến hành tích hợp vào Login Gate. Từ đó cung cấp login cho các dịch vụ Webpayment, Promotion, CS.
* Login Gate chủ động tích hợp với các kênh login để cung cấp bằng `UserPlatform` của GT từ đó cung cấp cho các dịch vụ Webpayment, Promotion, CS.

contact: LinhNN6

## 4. Tài liệu APIs tích hợp
### 4.1. Information required for integration

- Game name:
- Game shortname: $gameID
- Callback ID: $mapID
- Callback URL:
Social Info:
- Facebook Channel:
    + Facebook appID
    + Facebook app secret key
    + config Social Login Redirect URL to https://login.pp.m.zing.vn/login/fbcallback/$gameID
- Google Channel:
    + oAuth web clientID
    + oAuth web client secret key
    + config Authorized Redirect Urls: https://login.pp.m.zing.vn/login/ggcallback/$gameID
- Zalo Channel:
    + zalo appID
    + zalo app secretKey
    + config RedirectUrl: https://login.pp.m.zing.vn/login/ggcallback/$gameID
    + Android package name
    + Android hash key ( required if app use ZaloSDK)

### 4.2. Login Form URL Format

- Zing: ```https://login.pp.m.zing.vn/oauth/zing?appID=$gameID&mapID=$mapID```
- Facebook: ```https://login.pp.m.zing.vn/oauth/facebook?appID=$gameID&mapID=$mapID```
- Zalo: ```https://login.pp.m.zing.vn/oauth/zalo?appID=$gameID&mapID=$mapID```
- Google: ```https://login.pp.m.zing.vn/oauth/google?appID=$gameID&mapID=$mapID```
- Email: ```https://login.pp.m.zing.vn/oauth/email?appID=$gameID&mapID=$mapID```

* Output params:

    Param    |Type      |Description
    ---------|-------|-----------
    success  |Integer| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    loginType|String | Referring to loginType in [Appendix 2](#appendix-2-logintype)
    gameID   |String | $gameID
    appID    |String | $gameID
    userID   |String | User Identified Number
    session  |String | Session of user
    userName |String | 
    ts       |String | Time in milisecond
    sig      |String |```md5($SECRET_KEY +  $success + $gameID + $ session + $userID + $ loginType + $ts)```<br>```$SECRET_KEY```: will be providedafter register integration

### 4.3. API
#### 4.3.1 Check Session
    
* Url: https://login.pp.m.zing.vn/login/session/check
* Protocol: HTTP GET / POST
* Description: Check session in passport system (for web app), if your app uses other session, please check other docs.
* Input params:

    Param|Type|Description
    -----|----|-----------
    appID `required`|String|```$gameID```
    userID `required`|String|userID will be include in URL callback after successful login.
    sessionID `required`|String|sessionID will be included in URL callback after successful login
    userAgent `required`|String|user agent of browser/device
    timestamp `required`|String|time in milisecond
    sig `required`|String|```md5($SECRET_KEY + $gameID + $userID + $sessionID + $userAgent + $timestamp)```<br>```$SECRET_KEY```: will be providedafter register integration

* Output params:

    Param|Type|Description
    -----|----|-----------
    returnCode|Integer| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    message|String| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)

#### 4.3.2 Remove Session

* Url: https://login.pp.m.zing.vn/login/session/remove
* Protocol: HTTP GET / POST
* Description: Remove session in passport system (passport system can't remove permisson which accessed by user in social system).
* Input params:

    Param|Type|Description
    -----|----|-----------
    appID `required`|String|```$gameID```
    userID `required`|String|userID will be include in URL callback after successful login.
    sessionID `required`|String|sessionID will be included in URL callback after successful login
    timestamp `required`|String|time in milisecond
    sig `required`|String|```md5($SECRET_KEY + $gameID + $userID + $sessionID + $timestamp)```<br>```$SECRET_KEY```: will be providedafter register integration

* Output params:

    Param|Type|Description
    -----|----|-----------
    returnCode|Integer| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    message|String| Referring to returnCode and message in [Appendix 1](#appendix-1-returncode-and-message)
    
### Appendix 1. returnCode and message

* Description table
    
    returnCode|message
    ----|-------------
    0   | Success
    1   | Invalid signature
    2   | Invalid input data
    3   | Expired TTL
    4   | AppID not exists
    5   | MapID not exists
    9   | Api not supported
    100 | Back to login
    101 | Permission denied by social
    102 | Email exists
    103 | Email not exists
    104 | Non existed user
    106 | Invalid username or password
    107 | Social login error
    108 | Session invalid or expired
    109 | Login channel not supported
    301 | Invalid Configuration
    500 | Server error

### Appendix 2. loginType

* Description table
    
    returnCode|message
    ----|-------------
    9    | QUICK
    10   | GUEST
    11   | ZING
    12   | ZALO
    13   | GOOGLE
    14   | FACEBOOK
    15   | ZINGME
    16   | LINE

## 5. Hướng dẫn truy xuất thông tin user
### 5.1 Yêu cầu cấp quyền truy xuất thông tin user
Nhằm hỗ trợ các bên liên quan truy xuất thông tin user của các ứng dụng. GT cung cấp dịch vụ Passport Support trên tool GT Framework (https://framework.mto.zing.vn).

Để yêu cầu quản trị viên cấp quyền truy xuất thông tin user của bất kì ứng dụng nào cần mail về  gt.framework với những yêu cầu sau:

1. Tên và faCode của ứng dụng muốn truy xuất.
2. Domain nhân viên được cấp quyền.
3. CC người quản lý trược tiếp của ứng dụng muốn truy xuất trong mail yêu cầu.
4. Có sự xác nhận cho phép cấp quyền từ quản lý trược tiếp của ứng dụng muốn truy xuất.

### 5.2 Hướng dẫn truy xuất thông tin user.

Thực hiện các bước sau đây:

1. Truy cập đường link https://framework.mto.zing.vn/ppsupport/getProfile .
2. Tại khung "Sản phẩm" chọn sản phẩm muốn truy xuất thông tin user (Ở khung này sẽ chỉ hiển thị những sản phẩm đã được cấp quyền).
3. Tại khung "Dịch vụ" chọn dịch vụ với thông tin input và output mong muốn: Ví dụ Email=>GuestProfile: Dịch vụ cần thông tin email của user để lấy thông tin Guest user (Ở khung này sẽ chỉ hiện thị những dịch vụ đã được cấp quyền hoặc đối với sản phẩm có cung cấp).
4. Nhập thông tin input tương ứng. Có thể truy xuất nhiều user cùng 1 lúc bằng cách xuống dòng (ấn Enter) sau mỗi input
5. Nhấp nút "Xác nhận" hoặc tổ hợp phím **Ctrl + Enter**
![PPSupport Service](ppsupport-tool.png "Dịch vụ truy xuất thông tin user")



## 6. FAQs