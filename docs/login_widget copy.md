# Passport Login Widget (PLW)

## 1. Giới thiệu dịch vụ

PLW là đoạn mã giúp anh/chị có thể thực hiện xác thực thông tin với hệ thống Passport VNG thông qua dạng popup, được tích hợp đầy đủ các phương thức đăng nhập mà Passport VNG cung cấp.

## 2. PLW cung cấp các phương thức đăng nhập nào?

PLW cung cấp phương thức đăng nhập phổ biến như Zing, Email, Facebook, Google, Zalo, Apple.
![Login Widget](./login.png)

## 3. Làm thế nào để có thể tích hợp PLW?

Để có thể tích hợp dịch vụ PLW, anh/chị có thể gửi mail tới gt.framework@vng.com.vn để biết thêm về quy trình tích hợp.

## 4. Thông tin tích hợp PLW cần những gì?

Các thông tin cần thiết để có thể sử dụng PLW bao gồm:

- **mapID, appID**: Được GT cung cấp khi anh/chị yêu cầu sử dụng dịch vụ
- **methods**: Là các phương thức đăng nhập anh/chị đã đăng ký với GT bao gồm các phương thức sau: Zing, Email, Facebook, Google, Zalo, Apple
- **callback**: Là hàm nhận thông tin sau khi anh/chị thực hiện quá trình xác thực thông tin thông qua PLW.

## 5. Làm thế nào để tích hợp PLW vào trang?

Anh/chị chỉ cần chèn đoạn mã dưới đây vào trang là có thể sử dụng PLW

_Để phù hợp với giao diện, thiết kế trang của anh/chị. PLW chỉ cung cấp các hàm để anh/chị tương tác với PLW._

```js
// Initializing widget
window.widgetInit = () => {
  openWidget.init({
    appID: 'snkindo',
    mapID: 'vca',
    methods: ['email', 'facebook'],
    callback: 'doCallback' // Can be change to another function's name.
  })
}
// Function callback, that was registered in function initialized.
window.doCallback = data => {
  // do something
}

// Function để mở popup
openWidget.doAuthentication()

// Import fundamental JS


;(function(d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0]
  if (d.getElementById(id)) {
    window.widgetInit()
    return
  }
  js = d.createElement(s)
  js.id = id
  js.src = 'https://widget.id.zing.vn/static/js/openWidget.js?ts='+ new Date().getTime()
```

[**Bạn có thể sử dụng thử tại đây**](https://sandbox-pay.mto.zing.vn/widget)
