# TỔNG QUAN

## 1. Giới thiệu dịch vụ

GT Passport là sản phẩm cung cấp các dịch vụ về đăng kí, đăng nhập cho các sản phẩm VNG. VD: VNG Game Publishing, Zing Play, Esale …

## 2. Các hệ thống đăng nhập

<!-- **Login Mobile** [:link:](/GTPassport/mobile_login.md) -->

**Login Mobile**

Dịch vụ đăng nhập cung cấp cho game mobile, tích hợp với các 3rd User Platform (Zing, Facebook, Google ...)

<!-- **Login H5** [:link:](/GTPassport/h5_login.md) -->

**Login H5**

Dịch vụ đăng nhập cung cấp cho game h5, tích hợp với các 3rd User Platform (Zing, Facebook, Google ...)

<!-- **ZingID** [:link:](/GTPassport/zingid.md) -->

**ZingID**

Hệ thống Zing ID cung cấp login cho các game VNG, thường được sử dụng cho các game PC/Web

**Login Gateway** [:link:](/GTPassport/logingw)

Cổng đăng nhập trung gian kết nối đến các UserPlatform, Game

## 3. Passport Login Widget [:link:](/GTPassport/login_widget)

Dịch vụ xác thực thông tin người dùng với hệ thống Passport VNG thông qua dạng popup, được tích hợp đầy đủ các phương thức đăng nhập mà Passport VNG cung cấp

<!-- ## 3. Quy trình tích hợp: Liệt kê các quy trình đã được nêu trong các loại Login

## 4. Hỗ trợ các yêu cầu

Cung cấp truy xuất thông tin userID in-game. Link đến dashboard

## 5. FAQs -->
