# Passport Login Widget

_|_|
-------|-|
Version| 1.0
Release date| 05-08-2020
Description| Tài liệu tích hợp với Passport Login Widget cho các dịch vụ phục vụ game mobile

##Change log
Version | Date | Description |
-|-|-|
1.0 (Draft) | 05-08-2020 | Khởi tạo tài liệu |

## Giới thiệu

* Passport Login Widget là tính năng giúp các trang dịch vụ của game (vd: cs, promotion, web payment ...), cho phép user đăng nhập với Login Widget và nhận lại các thông tin cần thiết để định danh user khi cung cấp dịch vụ

* Sơ đồ hoạt động

![overview](files/gt-ppt-login-widget.jpg)

## Các bước tích hợp

### 1. Đăng kí 

Đăng kí thông tin tích hợp qua [GT Ticket](https://gt.vng.vn/ticket)

![create ticket](files/ticket-creating.png)

### 2. Khởi tạo widget

Các thông tin cần thiết để có khởi tạo Login Widget:

- **mapID, appID**: Được GT cung cấp khi anh/chị yêu cầu sử dụng dịch vụ
- **methods**: Là các phương thức đăng nhập dịch vụ cần sử dụng (lưu ý rằng game cũng đang sử dụng các phương thức đăng nhập tương ứng). Bao gồm các phương thức sau: Zing, Email, Facebook, Google, Zalo, Apple, Line
- **callback**: Là hàm nhận thông tin sau khi anh/chị thực hiện quá trình xác thực thông tin thông qua PLW.

```js
// Initializing widget
window.widgetInit = () => {
  openWidget.init({
    appID: 'snkindo',
    clientID: 'vca',
    methods: ['email', 'facebook'],
    callback: 'doCallback' // Can be change to another function's name.
  })
}
// Function callback, that was registered in function initialized.
window.doCallback = data => {
  // do something
}

// Function để mở popup
openWidget.doAuthentication()

// Import fundamental JS


;(function(d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0]
  if (d.getElementById(id)) {
    window.widgetInit()
    return
  }
  js = d.createElement(s)
  js.id = id
  js.src = 'https://widget.id.zing.vn/static/js/openWidget.js?ts='+ new Date().getTime()
```

Trang demo: [Web Payment Sandbox](https://sandbox-pay.mto.zing.vn/widget)

| ![widget](files/widget.png) |
| :--: | 


### 3. Validate code

  Lấy thông tin user từ code nhận được. Code chỉ có giá trị trong khoảng thời gian nhất định, và chỉ sử dụng được 1 lần và có thời hạn trong 2 giờ.

#### URL

` POST https://graph.id.zing.vn/auth/token/validateCode`

#### Tham số

clientID: string
: **(Required)**
ClientID được đăng ký với GT

code: string
: **(Required)**
Tham số được trả về sau khi widget của GT trả về

appID: string
: **(Required)**
Id của game cần đăng nhập

scope: string
: **(Optional)**
Những trường cần truyền về thêm, bao gồm: *appID*, *appID+type*, *none*. Default là ***all***

clientSecret: string
: **(Required)**
Một chuỗi JWT đại diện cho chữ kí cho client

#### Kết quả

error: number
: Error code
  
* 0 : success
* Khác : lỗi

message: string
: Thông tin thêm về kết quả lỗi

data: object
: Data response

- idToken: một chuỗi ***JWT*** chứa thông tin về tài khoản user
- tokenType: loại dữ liệu trả về. Hiện tại đang là *bearer_token*

### 4. Phát sinh một Client Secret

Một ***client secret*** là một chuỗi JWT, một tiêu chuẩn mở ([RFC 7519](https://tools.ietf.org/html/rfc7519)), được sử dụng nhằm giúp bảo mật cho việc truyền dữ liệu. Việc tạo một token cần một *shared_key* mà client đã đăng kí trước. Tham khảo thêm tại.

Phát sinh một chữ kí JWT cần thực hiện các việc sau:

1. Tạo JWT header.
2. Tạo JWT payload.
3. Kí chữ kí.

Để tạo một JWT, sử dụng trường và giá trị sau trong JWT hearder:

|  Header | Mô tả  |
|---|---|
| alg  | Thuật toán sử dụng. Hiện tại passport widget sử dụng **HS256**  |

JWT payload cần những thông tin đặc tả:

|  Header | Mô tả  |
|---|---|
| issuer | ID của issuer phát sinh JWT này. Hiện tại chưa sử dụng |
| iat | Thời gian phát sinh *client secret*, là một UNIX Time (millius giây) |
| exp | Thời gian hết hạn của *client secret*, là một UNIX Time. Một expire time hợp lệ nên là một giá trị lớn hơn ***iat*** ít nhất 60.000ms (2 phút).
| aud | Là đối tượng mà client secret muốn gửi đên (hoặc có liên quan). Hiện tại nên là `https://widget.login.pp.m.zing.vn` |
| sub | Là subject của *client secret*. Sử dụng cùng giá trị với clientID |

Sau khi tạo JWT, hãy kí chữ kí điện tử cho nó bằng thuật toán HS256 bằng *share_key* đã đăng kí với passport widget.

Một decoded JWT sẽ giống như sau :

```code
{
  "alg": "HS256"
}
{
  "aud": "https://login.pp.m.zing.vn",
  "sub": "vca-dev",
  "iat": 1596355919,
  "exp": 1596365919
}
```

### 5. Sử dụng thông tin IDToken

Trước khi sử dụng thông tin trả về từ GT Login Widget, client cần xác nhận IDToken phản hồi.

Các công việc mà client cần làm:

1. Xác nhận chữ kí của IDToken trả về. Dùng *IDTokenKey* được cung cấp.
2. Xác nhận *exp* của token.
3. Kiểm tra *iss*, giá trị nên là domain kiểu viết ngược `vn.zing.id`.
4. Kiểm tra aud, giá trị nên là *clientID*.

Một response hợp lệ khi trả về có dạng:

```codes
{
    "returnCode": 0,
    "data": {
        "idToken": "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ2bi56aW5nLmlkLXN0YWdpbmciLCJpYXQiOjE1OTY1NTg0NDgsImV4cCI6MTU5NjU4MDA0OCwiZ2FtZUlEIjoiZGVtb3ZuIiwibG9naW5UeXBlIjoiemluZyIsInVzZXJJRCI6IjE3MDIyNjk1MTYzNTgxMzE3MTIiLCJhdWQiOiJ2Y2Etc3RhZ2luZyJ9.xtpb-1xolIKZwBmcKN9fvk9myzmv_bv6f2AoTtWAg_0",
        "tokenType": "bearer_token"
    },
    "message": "Thành công"
}
```

Khi decode thành công, kết quả IDToken nên nhận được như sau:

```code
{
  "alg": "HS256"
}
{
  "iss": "vn.zing.id",
  "iat": 1596558448,
  "exp": 1596580048,
  "appID": "demovn",
  "loginType": "zing",
  "userID": "1702269516358131712",
  "aud": "vca"
}
```