##Passport Login API v2

_|_|
-------|-|
Version| 1.0
Release date| 17-07-2019
Description| This document specifies the Login APIs version 2 for mobile games

##Change log
Version|Description|
-|-|

##List APIs

_|Name|Description
-|----|-----------
1|[Create user Guest](#1-create-user-guest) | - 
2|[Create user Zalo](#2-create-user-zalo) |After login Zalo by Zalo SDK, client send socialID which is ```uid``` in Zalo's response to server to get UserID and session for game
3|[Create user Facebook](#3-create-user-facebook)|After login Facebook by SDK, client send socialID which is ```id``` in Facebook's response to server to get UserID and session for game
4|[Create user Google](#4-create-user-google)|After login Google by SDK, client send socialID which is ```id``` in Google's response to server to get UserID and session for game
5|[Login Zing](#5-login-zing)| -
6|[Create Game session for Guest](#6-create-game-session-for-guest)| -
7|[Create Game session for Facebook](#7-create-game-session-for-facebook)| -
8|[Create Game session for Google](#8-create-game-session-for-google)| -
9|[Create Game session for Zalo](#9-create-game-session-for-zalo)| -
10|[Create Game session for Zing](#10-create-game-session-for-zing)| -
11|[Mapping Guest Account With Email](#11-mapping-guest-account-with-email)| -
12|[Recovery Guest Account With Email](#12-recovery-guest-account-with-email)| -
13|[Forget Password](#13-forget-password)| -
14|[PartnerSession Check Session](#14-partnersession-check-session)| -
15|[PartnerSession Remove Session](#15-partnersession-remove-session)| -
16|[Update Ext Info](#16-update-ext-info)| -
17|[Create user Apple](#17-create-game-session-for-apple)| -
18|[Create Game session for Apple](#18-create-game-session-for-apple)| -
19|[ClientSession Check Session](#19-clientsession-check-session)| -
20|[ClientSession Remove Session](#17-clientsession-remove-session)| -

<div class="pagebreak"></div>

###1. Create user guest

* Url: https://pp.m.zing.vn/api/login/createUser/guest
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    **Serial**|**Parameter Name**|**Data Type**|**Description**
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**deviceID** `required`|String|Example: 515908AE_2288_4695_AF10_AFA12446D82A
    3|**timestamp** `required`|String|Current time in milliseconds
    4|**ver** `required`|String|Version of Login APIs
    5|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    **Parameter Name**|**Data Type**|**Description**
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"accountName" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"protected" : Boolean,```<br>&emsp;```"map" : Boolean,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###2. Create user Zalo

* Url: https://pp.m.zing.vn/api/login/createUser/zalo
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**socialID** `required`|String|uid which Zalo's response after login
    3|**oauthCode** `required` |String|Zalo code 
    4|**appID** `required` |String|ID of app in Zalo
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###3. Create user Facebook

* Url: https://pp.m.zing.vn/api/login/createUser/facebook
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**socialID** `required`|String|id which Facebook's response after login
    3|**oauthCode** `required` |String|fb ```access_token```
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###4. Create user Google

* Url: https://pp.m.zing.vn/api/login/createUser/google
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**socialID** `required`|String|id which Google's response after login
    3|**oauthCode** `required` |String|GG ```idToken```
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###5. Login Zing

* Url: https://pp.m.zing.vn/api/login/auth/zing
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**u** `required`|String|Username
    3|**p** `required` |String|Password is hashed by MD5
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"dob" : long,```<br>&emsp;```"accountName" : String,```<br>&emsp;```"oauthCode" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###6. Create Game session for Guest

* Url: https://pp.m.zing.vn/api/login/createGameSession/guest
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**userID** `required`|String|UserID
    3|**appID** `required` |String|App_ID
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"protected" : Boolean,```<br>&emsp;```"map" : Boolean,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###7. Create Game session for facebook

* Url: https://pp.m.zing.vn/api/login/createGameSession/facebook
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**userID** `required`|String|UserID
    3|**oauthCode** `required`|String|fb ```access_token```
    4|**appID** `required` |String|App_ID
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###8. Create Game session for Google

* Url: https://pp.m.zing.vn/api/login/createGameSession/google
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**userID** `required`|String|UserID
    3|**oauthCode** `required`|String|GG ```idToken```
    4|**appID** `required` |String|App_ID
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###9. Create Game session for Zalo

* Url: https://pp.m.zing.vn/api/login/createGameSession/zalo
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**userID** `required`|String|UserID
    3|**oauthCode** `required`|String|Zalo code
    4|**appID** `required` |String|App_ID
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###10. Create Game session for Zing

* Url: https://pp.m.zing.vn/api/login/createGameSession/zing
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**userID** `required`|String|UserID
    3|**oauthCode** `required`|String|oauthCode in Zing's response
    4|**appID** `required` |String|App_ID
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"accountName" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###11. Mapping Guest Account With Email

* Url: https://pp.m.zing.vn/api/login/mapGuestEmail
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**userID** `required`|String|UserID
    3|**email** `required`|String|Email
    4|**password** `required` |String|Password is hashed by MD5
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"email" : String,```<br>&emsp;```"accountName" : String```<br>```}```

###12. Recovery Guest Account With Email

* Url: https://pp.m.zing.vn/api/login/recoverGuestEmail
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**email** `required`|String|Email
    3|**password** `required` |String|Password is hashed by MD5
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"email" : String,```<br>&emsp;```"accountName" : String,```<br>&emsp;```"sessionID" : String```<br>```}```

###13. Forget Password

* Url: https://pp.m.zing.vn/api/login/forgetPassword
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**email** `required`|String|Email
    3|**timestamp** `required`|String|Current time in milliseconds
    4|**ver** `required`|String|Version of Login APIs
    5|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|```null```

###14. PartnerSession Check Session

* Url: https://pp.m.zing.vn/api/login/checkSession
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**userID** `required`|String|UserID
    3|**sessionID** `required`|String|SessionID
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|```null```

###15. PartnerSession Remove Session

* Url: https://pp.m.zing.vn/api/login/removeSession
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**userID** `required`|String|UserID
    3|**sessionID** `required`|String|SessionID
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|```null```

###16. Update ext info

* Url: https://pp.m.zing.vn/api/login/updateExtInfo
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**userID** `required`|String|UserID
    3|**sessionID** `required`|String|SessionID
    4|**extInfo** `required` |String|extInfo is encrypted by The Advanced Encryption Standard (AES). Referring to extInfo Param in [Appendix 3](#appendix-3-extinfo-param)
    5|**timestamp** `required`|String|Current time in milliseconds
    6|**ver** `required`|String|Version of Login APIs
    7|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|```null```

<div class="pagebreak"></div>

###17. Create user Apple

* Url: https://pp.m.zing.vn/api/login/createUser/apple
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**oauthCode** `required` |String|apple ```authorization_code```
    3|**timestamp** `required`|String|Current time in milliseconds
    4|**ver** `required`|String|Version of Login APIs
    5|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"accessToken" : String,```<br>&emsp;```"refreshToken" : String,```<br>&emsp;```"idToken" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###18. Create Game session for Apple

* Url: https://pp.m.zing.vn/api/login/createGameSession/apple
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|GameID
    2|**userID** `required`|String|UserID
    3|**oauthCode** `required`|String|apple ```authorization_code```
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{```<br>&emsp;```"userID" : String,```<br>&emsp;```"sessionID" : String,```<br>&emsp;```"socialID" : String,```<br>&emsp;```"accessToken" : String,```<br>&emsp;```"refreshToken" : String,```<br>&emsp;```"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}``

###19. ClientSession Check Session

* Url: https://pp.m.zing.vn/api/login/checkClientSession
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**userID** `required`|String|UserID
    3|**sessionID** `required`|String|SessionID
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|Format: <br>```{"extInfo" : JSON Object```[(?)](#appendix-4-extinfo-in-response)<br>```}```

###20. ClientSession Remove Session

* Url: https://pp.m.zing.vn/api/login/removeClientSession
* Protocol: HTTP POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Serial|Parameter Name|Data Type|Description
    :---|:-------------|:--------|:----------
    1|**gameID** `required`|String|Game/App ID
    2|**userID** `required`|String|UserID
    3|**sessionID** `required`|String|SessionID
    4|**timestamp** `required`|String|Current time in milliseconds
    5|**ver** `required`|String|Version of Login APIs
    6|**sig** `required`|String|Signature. Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

<div class="pagebreak"></div>

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code which Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    message|String|Error description. Referring to code and message in [Appendix 2](#appendix-2-return-code-and-message)
    data|JSON Object|```null```    

##Appendix 1. Create signature

* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* SigHash Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* Package Name is name of a game's package

* All parameters except the signature itself and param ```ver``` of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort ascending on parameter ```Serial```.

* All parameters that form the signature must in their original Format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive. 

* All leading and trailing whitespace of a string will have to be trimmed.  

* Example:
    Create signature for API Create user guest  

    ```
    URL: "https://$domain/$uri?gameID=game&deviceID=device&timestamp=1517900546676&ver=v2&sig=";
    SigHash Key: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    Package Name: "pg1.vng.tlbb"
    Raw: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "pg1.vng.tlbb" + "game" + "device" + "1517900546676"
    Sig: md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcpg1.vng.tlbbgamedevice1517900546676") = "20e099815783735d5e166ff8b9f09ca1"
    ```

##Appendix 2. Return code and message

* Description Table

    Code    |Message
    :----   |:----------
    1       |Thành công
    -1      |Lỗi sai signature
    -2      |Lỗi sai dữ liệu
    -102    |Email đã được sử dụng
    -103    |Email không tồn tại
    -104    |Tài khoản đã được bảo vệ trước đó
    -105    |User không tồn tại
    -107    |```oauthCode``` hết hạn / không chính xác
    -108    |Game session không chính xác
    -404    |Không tìm thấy action
    -500    |Lỗi Server
    ...|...

##Appendix 3. extInfo Param
* extInfo Struct
    ```
    {
      "ppDOI": {
        "index": 4,
        "text": "Ngày cấp CMND",
        "type": "long",
        "value": 0,
        "required": false
      },
      "ppPOI": {
        "index": 5,
        "text": "Nơi cấp CMND",
        "type": "String",
        "value": "",
        "required": false
      },
      "phone": {
        "index": 128,
        "text": "Điện thoại",
        "type": "phonestring",
        "value": "",
        "required": true
      },
      "dob": {
        "index": 2,
        "text": "Ngày sinh",
        "type": "long",
        "value": 0,
        "required": false
      },
      "name": {
        "index": 1,
        "text": "Họ tên",
        "type": "String",
        "value": "",
        "required": true
      },
      "addr": {
        "index": 6,
        "text": "Địa chỉ",
        "type": "String",
        "value": "",
        "required": false
      },
      "email": {
        "index": 7,
        "text": "Email",
        "type": "email",
        "value": "",
        "required": false
      },
      "ppID": {
        "index": 3,
        "text": "CMND",
        "type": "String",
        "value": "",
        "required": false
      }
    }
    ```

* Java code

    ```java
    package zcore.utilities;

    import java.io.UnsupportedEncodingException;
    import java.nio.charset.Charset;
    import java.security.GeneralSecurityException;
    import javax.crypto.Cipher;
    import javax.crypto.spec.IvParameterSpec;
    import javax.crypto.spec.SecretKeySpec;
    import org.apache.commons.codec.DecoderException;
    import org.apache.commons.codec.binary.Base64;
    import org.apache.commons.codec.binary.Hex;
    
    public class CryptionUtil {
    
       public static String encrypt(String key, String strPlain)
                throws GeneralSecurityException, UnsupportedEncodingException, DecoderException {
                
            byte[] raw = key.getBytes(Charset.forName("UTF-8"));
            if (raw.length != 16) {
                throw new IllegalArgumentException("Invalid key size.");
            }
    
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
            
            byte[] doFinal = cipher.doFinal(strPlain.getBytes(Charset.forName("UTF-8")));
            return new String(Base64.encodeBase64(cipher.doFinal(strPlain.getBytes(Charset.forName("UTF-8")))), Charset.forName("UTF-8"));
    
        }
    
        public static String decrypt(String key, String strEncrypted)
                throws GeneralSecurityException, DecoderException {
                
            byte[] encrypted = Base64.decodeBase64(strEncrypted);
    
            byte[] raw = key.getBytes(Charset.forName("UTF-8"));
            if (raw.length != 16) {
                throw new IllegalArgumentException("Invalid key size.");
            }
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
    
            byte[] original = cipher.doFinal(encrypted);
            return new String(original, Charset.forName("UTF-8"));
        }
    
    
        public static void main(String[] args) throws GeneralSecurityException, UnsupportedEncodingException, DecoderException {
            String key = "7RmcNvsrhXGt4K4L";
            System.out.println(key.length());
            System.out.println("key: " + key);
            String encrypt = encrypt(key, "{\"locth2\":\"hehe\"}");
            System.out.println("encrypt: " + encrypt);
            System.out.println("decrypt: " + decrypt(key, encrypt));
        }
    }
    ```


##Appendix 4. extInfo in response
    {
        "info":String,
        "bypass": Boolean
    }

* Description Table

    Element Name|Description
    :---------|:------------
    info|Ext Profile is encrypted
    bypass|Get Skip ```ext_profile``` in Config

